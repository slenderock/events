Rails.application.routes.draw do
  get 'events/index'

  get 'groups/index'

  root 'home#index'

  devise_for :users

  get 'home/index'

  resources :posts do
    resources :comments, only: [:index, :new, :create] do
      member { post :vote }
    end
  end
end
