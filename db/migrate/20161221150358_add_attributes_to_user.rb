class AddAttributesToUser < ActiveRecord::Migration[5.0]
  def change
    [:lname, :fname, :phone, :skype, :site, :description, :image].each do |column_name|
      add_column :users, column_name, :string
    end
  end
end
